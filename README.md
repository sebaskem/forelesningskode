# TDT4109 Forelesningskode

Børge sin kode brukt på forelesning og ellers i faget

For å klone denne på egen maskin så er det laget litt materiell om git [her](https://itgkmat.apps.stack.it.ntnu.no/user/cb1f1e93-9cde-4a58-88f9-a76334e09ff1/notebooks/materiale/JN7/JN7_git.ipynb). Alternativt kan du bare hente ned koden fra en enkelt uke som en zipfil.

Det er også mulig å trykke på én pythonfil og kjøre denne i *gitpod*. Du vil da få opp en utviklingsomgivelse (VS Code) i nettleseren din, så du kan kjøre filen der. Det er ikke sikkert at det vil virke for alle tilfeller, eksempelvis fordi ikke alle moduler er installert. 