# Dette benytter seg av 'walrus operator'. Noen liker det, andre ikke. Det kan lett bli uoversiktlig,
# samtidig slipper man å kickstarte en whileløkke før man hopper inn i den.

while alder := int(input("tall: ")) < 100:
    print("For lavt")