# Print håndterer en variabel mengde parametre

# Legg ellers merke til hvordan en kan fortsette en lang greie på neste linje ved
# å slenge på en \ til slutt

print('Dette', 'er', 'en', 'helt', 'grei', 'ting', 'å', 'sende', 'inn', 'til', \
      'print,', 'fordi', 'den', 'rett', 'og', 'slett', 'er', 'veldig', 'flink', \
      'til', 'å', 'huske,', 'tror', 'jeg?')

# Men hvordan gjør den det?! Livredderen er en liten, vakker stjerne

def skriv(*args):
    print(args)
    print(type(args))
    
#     for part in args:
#         print(part,end="_")
# Men ser dere den siste _ etter 'jeg?'? Den skulle jo ikke være der.    
#    print("%".join(args))
    
skriv('Dette', 'er', 'en', 'helt', 'grei', 'ting', 'å', 'sende', 'inn', 'til', \
      'print,', 'fordi', 'den', 'rett', 'og', 'slett', 'er', 'veldig', 'flink', \
      'til', 'å', 'huske,', 'tror', 'jeg?')

skriv('dette', ['er', ['også'], ('mulig?', 1), True]) # parametrene kan være av ymse typer...
# Hva er et (tuppel)? Det er som en liste, men den er 'immutable' - ikke-muterbar
# Det betyr at man ikke kan endre innholdet i den. Append og slikt finnes ikke.
# Det sparer minne og er litt raskere, og egentlig bør man bruke dem når man uansett
# ikke skal endre data.


# La oss se et annet eksempel, bruk av en egendefinert funksjon fra en egen modul.
import min_modul 

print(min_modul.legg_sammen(1,2,3))
